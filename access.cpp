// in this programe, we are going to test if the variable published by publish can be accessed


#include "boost/interprocess/shared_memory_object.hpp"
#include "boost/interprocess/mapped_region.hpp"
#include <iostream>
#include "boost/interprocess/exceptions.hpp"
using namespace boost::interprocess;


int main() {
	//we want to throw a ecception if the shared memory does not exist
	try {
		//creat an open only shared memory object,and use it to access "Shared_Memory"
		shared_memory_object shm_obj2{ open_only, "Shared_Memory", read_write };
		//get access to object from this program
		mapped_region read_int{ shm_obj2,read_only };
		// initialized an integer with that address
		int *i = static_cast<int*>(read_int.get_address());
		//print the integer
		std::cout << "\n the shared integer is :" << *i << std::endl;
		//deleted the memory
		shm_obj2.remove("Shared_Memory");
		std::cin.get();
	}
	
	catch (boost::interprocess::interprocess_exception)
	{
		std::cout << "Shared memory is removed" << std::endl;
		std::cin.get();
		return 0;
	}









}
