# README #

This is the SOA and Product Service homework for MTH9815: Software Engineering for Finance at Baruch MFE
Team Members: Huiyou Chen & Weihao Li

### P1: Shared Memory Application ###
For exercise 1, there are two .cpp files: publish.cpp and access.cpp. 
To test the program, you may need to run "publish.cpp" first, which will publish an integer. 
Then, you can run "access.cpp", which will print out the interger published.


### P2P3: Product Service Application ###
For exercise 2 and 3, test scripts have been implemented in main() function in "test_products.cpp" file.
You can just run "test_products.cpp" to show all print out stuff.