// this program will create a shared memory object, where the integer is saved

// To test, run "publish.cpp" first and the "access.cpp"
#include "boost/interprocess/shared_memory_object.hpp"
#include "boost/interprocess/mapped_region.hpp"

using namespace boost::interprocess;

int main() {
	// create the memory
	shared_memory_object shm_obj(open_or_create, "Shared_Memory", read_write);
	// resized the memory
	shm_obj.truncate(sizeof(int));
	//map the address of the memory to the application
	mapped_region shr_obj(shm_obj, read_write);
	//create a pointer pointing to that memory
	int *i = static_cast<int*>(shr_obj.get_address());
	*i = 1992;
	
	
	



}
