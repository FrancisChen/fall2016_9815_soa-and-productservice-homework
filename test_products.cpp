/**
 * test program for our ProductServices
 */

#include <iostream>
#include "products.hpp"
#include "productservice.hpp"

using namespace std;

int main()
{
  // Create the 10Y treasury note
  date maturityDate(2026, Nov, 16);
  string cusip = "912828M56";
  Bond treasuryBond(cusip, CUSIP, "T", 2.25, maturityDate);

  // Create the 2Y treasury note
  date maturityDate2(2018, Nov, 5);
  string cusip2 = "912828TW0";
  Bond treasuryBond2(cusip2, CUSIP, "T", 0.75, maturityDate2);

  // Create a BondProductService
  BondProductService *bondProductService = new BondProductService();

  // Add the 10Y note to the BondProductService and retrieve it from the service
  bondProductService->Add(treasuryBond);
  Bond bond = bondProductService->GetData(cusip);
  cout << "CUSIP: " << bond.GetProductId() << " ==> " << bond << endl;

  // Add the 2Y note to the BondProductService and retrieve it from the service
  bondProductService->Add(treasuryBond2);
  bond = bondProductService->GetData(cusip2);
  cout << "CUSIP: " << bond.GetProductId() << " ==> " << bond << endl;

  // Create the Spot 10Y Outright Swap
  date effectiveDate(2016, Nov, 16);
  date terminationDate(2026, Nov, 16);
  string outright10Y = "Spot-Outright-10Y";
  IRSwap outright10YSwap(outright10Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate, terminationDate, USD, 10, SPOT, OUTRIGHT);

  // Create the IMM 2Y Outright Swap
  date effectiveDate2(2016, Dec, 20);
  date terminationDate2(2018, Dec, 20);
  string imm2Y = "IMM-Outright-2Y";
  IRSwap imm2YSwap(imm2Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate2, terminationDate2, USD, 2, IMM, OUTRIGHT);

  // Create a IRSwapProductService
  IRSwapProductService *swapProductService = new IRSwapProductService();

  // Add the Spot 10Y Outright Swap to the IRSwapProductService and retrieve it from the service
  swapProductService->Add(outright10YSwap);
  IRSwap swap = swapProductService->GetData(outright10Y);
  cout << "Swap: " << swap.GetProductId() << " == > " << swap << endl;

  // Add the IMM 2Y Outright Swap to the IRSwapProductService and retrieve it from the service
  swapProductService->Add(imm2YSwap);
  swap = swapProductService->GetData(imm2Y);
  cout << "Swap: " << swap.GetProductId() << " == > " << swap << endl;
    
    
    // HOMEWORK CODES STARTING FROM HERE
    // Create a EuroDollarFuture
    date EuroDollarFutureMaturity(2016, Dec, 20); //EuroDollar Future Maturity: Second London bank business day before 3rd Wednesday of the contract month.
    string eurodollarfutureId = "ED-DEC-2016";
    EuroDollarFuture euro_dollar_future(eurodollarfutureId, EuroDollarFutureMaturity, 1e6, 25, 1013, 750, 99.02); // Data quotes from CME
    
    // Create a BondFuture
    date BondFutureMaturity(2017, Mar, 23); //U.S. Treasury Bond Future Maturity: Seventh business day preceding the last business day of the delivery month
    string bondfutureId = "BF-MAR-2017";
    BondFuture bond_future(bondfutureId, BondFutureMaturity, 1e5, 31.25, 3375, 2500, 151.84); // Data quotes from CME
    
    // Create a FutureProductService
    FutureProductService *future_product_service = new FutureProductService();
    
    // Add the EuroDollarFuture to the FutureProductService and retrieve it from the service
    future_product_service->Add(euro_dollar_future);
    Future future = future_product_service->GetData(eurodollarfutureId);
    cout << "Eurodollar Future: " << future.GetProductId() << " ==> " << future << endl;
    cout << future.GetProductId() << " ==> " << euro_dollar_future << endl;

    // Add the BondDollar to the FutureProductService and retrieve it from the service
    future_product_service->Add(bond_future);
    future = future_product_service->GetData(bondfutureId);
    cout << "Bond Future: " << future.GetProductId() << " ==> " << future << endl;
    cout << future.GetProductId() << " ==> " << bond_future << endl;
    
    //test Get functions
    vector<Bond> bonds = bondProductService->GetBonds("T");
    cout << "Number of Bonds with ticker T: " << bonds.size() << endl;
    
    vector<IRSwap> swaps = swapProductService->GetSwaps(THIRTY_THREE_SIXTY);
    cout << "Number of Swaps with day count convention of THIRTY_THREE_SIXTY: " << swaps.size() << endl;
    
    swaps = swapProductService->GetSwaps(SEMI_ANNUAL);
    cout << "Number of Swaps with payment frequency of SEMI_ANNUAL: " << swaps.size() << endl;
    
    swaps = swapProductService->GetSwaps(LIBOR);
    cout << "Number of Swaps with floating index of LIBOR: " << swaps.size() << endl;
    
    swaps = swapProductService->GetSwapsGreaterThan(2);
    cout << "Number of Swaps with term years greater than 2: " << swaps.size() << endl;
    
    swaps = swapProductService->GetSwapsLessThan(5);
    cout << "Number of Swaps with term years less than 5: " << swaps.size() << endl;
    
    swaps = swapProductService->GetSwaps(IMM);
    cout << "Number of Swaps with swap type IMM: " << swaps.size() << endl;
    
    swaps = swapProductService->GetSwaps(OUTRIGHT);
    cout << "Number of Swaps with swap leg type OUTRIGHT: " << swaps.size() << endl;
    
  return 0;
}
